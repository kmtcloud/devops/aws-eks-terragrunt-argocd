const mockTodoListData = [{
    id: 1,
    text: 'mock todo',
    isChecked: false

}, {
    id: 2,
    text: 'mock todo 2',
    isChecked: true

}];

export { mockTodoListData };
