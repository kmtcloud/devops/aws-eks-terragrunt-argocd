import { mockApi } from '../mockApi';

const Nightmare = require('nightmare')
const nightmare = Nightmare({
    show: true, waitTimeout: 10000, loadTimeout: 10000, executionTimeout: 10000
});
const domain = process.env.NODE_ENV === 'test' ? 'http://localhost:3000' : 'http://167.71.254.102:3001';
describe('e2e Tests', () => {
    beforeAll(() => {
        mockApi.mockGetTodos();
    });

    async function getTodoListCount() {
        return await nightmare.evaluate(() => {
            return document.querySelectorAll('.todo-list li').length;
        });

    }

    async function getRandomIndexFromTodos() {
        return Math.floor(Math.random() * await getTodoListCount());
    }

    it('should load todo items', async () => {
        await nightmare.goto(domain).wait('.todo-list');
        expect(await getTodoListCount()).toEqual(3);
    });
    it('should add new todo item', async () => {
        await nightmare.type('.add-todo-input', 'New todo item').click('.add-todo-button').wait(1000);
        expect(await getTodoListCount()).toEqual(4);
    });

    it('should mark & unmark as completed a todo item', async () => {
        const randomItemIndex = await getRandomIndexFromTodos();
        await nightmare.click(`.todo-list li:nth-child(${randomItemIndex + 1}) span`);
        const isExists = await nightmare.exists(`.todo-list li:nth-child(${randomItemIndex + 1}) span.todo-item__text--completed`)
        expect(isExists).toBeTruthy();
        await nightmare.wait(2000);
        await nightmare.click(`.todo-list li:nth-child(${randomItemIndex + 1}) span`);
        const isUnMarked = await nightmare.evaluate(randomItemIndex => {
            return document.querySelector(`.todo-list li:nth-child(${randomItemIndex + 1}) span.todo-item__text--completed`) === null;
        }, randomItemIndex);
        expect(isUnMarked).toBeTruthy();
    }, 15000);

    it('should delete a todo item', async () => {
        await nightmare.wait(1500);
        const randomItemIndex = await getRandomIndexFromTodos();
        const itemCount = await getTodoListCount();
        await nightmare.click(`.todo-list li:nth-child(${randomItemIndex + 1}) button.delete-button`);
        expect(await getTodoListCount()).toEqual(itemCount - 1);
    }, 15000);
    it('should final', done => {
        nightmare.wait(2000).end().then(done);
    });
})
