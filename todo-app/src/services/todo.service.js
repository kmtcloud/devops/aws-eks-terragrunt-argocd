const domain = process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test' ? 'http://localhost:8080' : 'http://167.71.254.102:3002';
const baseUrl = `${domain}/api/todos`;

export const getTodoListDoReq = async () => {

    const response = await fetch(baseUrl, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    });
    return await response.json()
}

export const addTodoDoReq = async (payload, endpoint) => {
    const response = await fetch(baseUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    });
    return await response.json()
}


export const removeTodoDoReq = async (id) => {
    const response = await fetch(`${baseUrl}/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
        },
    });
    return await response.json()
}

export const toggleTodoDoReq = async (id) => {
    const response = await fetch(`${baseUrl}/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
    });
    return await response.json()
}
