class Todo {
    constructor(id, text, isChecked) {
        this.text = text;
        this.isChecked = isChecked;
        this.id = id;
    }
}

export default Todo;
