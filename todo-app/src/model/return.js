class ReturnRes {
    constructor( message, todos) {
        this.message = message;
        this.todos = todos;
    }
}
export default ReturnRes;
