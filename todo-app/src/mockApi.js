import fetchMock from 'fetch-mock'

class MockApi {
    baseUrl = 'http://localhost:8080/api/todos';

    constructor() {
        fetchMock.config.overwriteRoutes = true;
    }

    mockGetTodos() {
        if( process.env.NODE_ENV !== 'development')
            return
        const todos = [
            {
                id: 1,
                text: 'Todo 1',
                isChecked: false
            },
            {
                id: 2,
                text: 'Todo 2',
                isChecked: false
            },

            {
                id: 3,
                text: 'Todo 3',
                isChecked: false
            }
        ]
        fetchMock.get(this.baseUrl, todos);
    }

    mockAddTodo(text) {
        if( process.env.NODE_ENV !== 'development')
            return
        fetchMock.post(this.baseUrl, {
            todos: {
                text,
                id: new Date().getTime(),
                isChecked: false
            }
        })
    }

    mockDeleteTodo(id) {
        if( process.env.NODE_ENV !== 'development')
            return
        fetchMock.delete(`${this.baseUrl}/${id}`, { message: `Item deleted with given id: ${id}` })
    }

    mockToggleTodo(id) {
        if( process.env.NODE_ENV !== 'development')
            return
        fetchMock.put(`${this.baseUrl}/${id}`, { message: `Item marked with given id: ${id}` })
    }
}

export const mockApi = new MockApi();
