import React from "react";
import { getTodoList } from '../../redux/actions';
import Todo from '../todo/todo';
import { getAllTodos } from '../../redux/selector';
import { useDispatch, useSelector } from '../../redux/react-redux-hooks';

const TodoList = () => {
    const dispatch = useDispatch();
    React.useEffect(() => {
        dispatch(getTodoList());
    }, [dispatch]);

    const todos = useSelector(getAllTodos);
    return (
        <ul className="todo-list">
            {todos && todos.length > 0
                ? todos.map((todo, index) => {
                    return <Todo key={`todo-${todo.id}`} todo={todo} />;
                })
                : "Her şey tamam patron!"}
        </ul>
    );
};

export default TodoList;
