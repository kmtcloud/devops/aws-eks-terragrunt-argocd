import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import TodoList from './index';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { mockTodoListData } from '../../../test/mockData';
import * as ReactReduxHooks from '../../redux/react-redux-hooks';

let mockStore = configureStore([thunk])

configure({adapter: new Adapter()});
describe('TodoList component', () => {
    let wrapper;
    let store;
    beforeEach(() => {
        //will refactor later
        //mock store
        store = mockStore([]);
        jest
            .spyOn(ReactReduxHooks, 'useSelector')
            .mockImplementation(state => store.getState());
        /* mocking useDispatch on our mock store  */
        jest
            .spyOn(ReactReduxHooks, 'useDispatch')
            .mockImplementation(() => store.dispatch);

        wrapper = shallow(<TodoList store={store}/>);
    })

    it('Should render todo list comp properly', () => {
        expect(wrapper.exists()).toEqual(true);

    });
    //
    it('Should list todo items properly when 2 todo items is passed', () => {
        store = mockStore(mockTodoListData)
        wrapper.setProps()
        wrapper.update()
        expect(wrapper.find('.todo-list').children().length).toEqual(2);
    });
    //
    it('Should display her sey tamam patron when no todo exists', async () => {
        store = mockStore([])
        wrapper.setProps()
        wrapper.update()

        expect(wrapper.text('.todo-list')).toEqual('Her şey tamam patron!');
    });
});
