import React, { useState } from "react";
import { addTodo } from '../../redux/actions';
import { useDispatch } from '../../redux/react-redux-hooks';

const AddTodo = () => {
    const [input, setInput] = useState("");
    const dispatch = useDispatch();

    return (
        <div >
            <input className="add-todo-input" value={input} onChange={(e) => setInput(e.target.value)} />
            <button className="add-todo-button" onClick={() => {
                dispatch(addTodo(input))
                setInput('')
            }}>
                Add Todo
            </button>
        </div>
    );
};

export default AddTodo;
