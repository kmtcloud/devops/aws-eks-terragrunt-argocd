import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as ReactReduxHooks from '../../redux/react-redux-hooks';
import AddTodo from './index';

let mockStore = configureStore([thunk])

configure({adapter: new Adapter()});
describe('AddTodo component', () => {
    let wrapper;
    let store;
    beforeEach(() => {
        //will refactor later
        //mock store
        store = mockStore([]);
        // mocking useSelector on our mock store
        jest
            .spyOn(ReactReduxHooks, 'useSelector')
            .mockImplementation(state => store.getState());
        /* mocking useDispatch on our mock store  */
        jest
            .spyOn(ReactReduxHooks, 'useDispatch')
            .mockImplementation(() => store.dispatch);

        wrapper = shallow(<AddTodo store={store}/>);
    })
    it('Should render AddTodo component properly', () => {
        expect(wrapper.exists()).toEqual(true);
    });
    it('Should have one input to add new todo item', () => {
        expect(wrapper.children().find('.add-todo-input').length).toEqual(1);
    });
    //
    it('Should exist', () => {
        expect(wrapper.childAt(1).find('.add-todo-button').length).toEqual(1);
    });
});
