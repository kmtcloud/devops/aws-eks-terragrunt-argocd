import React from 'react';
import cx from 'classnames';
import { removeTodo, toggleTodo } from '../../redux/actions';
import { useDispatch } from '../../redux/react-redux-hooks';

const Todo = ({todo}) => {
    const dispatch = useDispatch();
    return (
        <li className="todo-item">
            {todo && todo.isChecked ? '👌' : '👋'}{' '}
            <div>
            <span onClick={() => dispatch(toggleTodo(todo.id))}
                  className={cx(
                      'todo-item__text',
                      todo && todo.isChecked && 'todo-item__text--completed'
                  )}
            >
        {todo.text}
            </span>
                <button className="delete-button" onClick={() => dispatch(removeTodo(todo.id))}
                        style={{marginLeft: 10}}>Delete
                </button>
            </div>

        </li>
    );
};

export default Todo;
