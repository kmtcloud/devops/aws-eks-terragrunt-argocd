import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { mockTodoListData } from '../../../test/mockData';
import * as ReactReduxHooks from '../../redux/react-redux-hooks';
import Todo from './todo';

let mockStore = configureStore([thunk])

configure({adapter: new Adapter()});
describe('Todo item component', () => {
    let wrapper;
    let store;
    beforeEach(() => {
        //will refactor later
        //mock store
        store = mockStore(mockTodoListData)
        // mocking useSelector on our mock store
        jest
            .spyOn(ReactReduxHooks, 'useSelector')
            .mockImplementation(state => store.getState());
        /* mocking useDispatch on our mock store  */
        jest
            .spyOn(ReactReduxHooks, 'useDispatch')
            .mockImplementation(() => store.dispatch);

        wrapper = shallow(<Todo todo={{text:"selam",id:2,isChecked:false}}/>);
    })

    it('Should render todo  comp properly', () => {
        expect(wrapper.exists()).toEqual(true);

    });
    //
    it('Should render todo item properly when a todo exists', () => {

        expect(wrapper.childAt(2).childAt(1).find('.delete-button').text()).toEqual('Delete');
        expect(wrapper.childAt(2).childAt(0).find('.todo-item__text').text()).toEqual('selam');

    });

});
