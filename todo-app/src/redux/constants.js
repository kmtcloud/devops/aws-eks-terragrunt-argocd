export const GET_TODOS = 'GET_TODOS';
export const SUBMIT_TODO = 'SUBMIT_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
