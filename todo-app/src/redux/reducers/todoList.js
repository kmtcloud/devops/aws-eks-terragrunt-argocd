import { DELETE_TODO, GET_TODOS, SUBMIT_TODO, TOGGLE_TODO } from '../constants';

const defaultState = [];
const todoList = (state = defaultState, action) => {
    switch (action.type) {
        case SUBMIT_TODO: {
            return [...state, { ...action.payload }];
        }
        case GET_TODOS: {
            return [...state, ...action.payload];
        }
        case DELETE_TODO: {
            return [...state.filter(todo=>todo.id !== action.id)];
        }
        case TOGGLE_TODO: {
            const foundTodo = state.find((e) => e.id === action.id);
            if (!foundTodo) {
                throw new Error(
                    "Unexpected error todo not found with given id " + action.id
                );
            }
            return [
                ...state.map((todo) => {
                    if (todo.id === action.id) {
                        return { ...todo, isChecked: !todo.isChecked };
                    }
                    return todo;
                })
            ];
        }
        default:
            return state;
    }
};

export default todoList;
