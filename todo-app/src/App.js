import React from "react";
import "./App.css";
import AddTodo from './components/addTodo';
import TodoList from './components/todoList';

export default function TodoApp() {
    return (
        <div className="todo-app">
            <h1>Todo List</h1>
            <AddTodo />


            <TodoList />
        </div>
    );
}
