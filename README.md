# 0. Desired Architecture
In this repo, we are covering implementing failover capability-enabled EKS cluster with Terragrunt and to manage applications that running on top of the EKS with ArgoCD.
Please check the following diagram.

![](images/architecture.jpeg)
 Overall Diagram

![](images/argocd-apps.png)
ArgoCD Applications

# 1. Instructions
We are managing several resources with different tools and services.
Aws related resources such as;
* VPC,
* EKS,
* Security groups,
* RDS,
* IAM Policies and Roles,
* ALB,
* WAF,
* Global Accelerator,

and helm releases such as;
* ArgoCD,
* cluster-auto-scaler,
* gitlab-runner,
* ingress-nginx

are managed by `Terraform` with `Terragrunt` wrapper.

And also, Kubernetes related resources such as; 
* Namepspaces(todo-app,sealed-secrets),
* Metrics-server,
* Sealed-secrets,
* Todo-app

are managed by ArgoCD.


## 2. Folder Structure
This repo has 4 main folders as follows.
* `.cd`: Contains pipeline and todo-app deployment configurations
* `aws`: Contains Terraform managed AWS resource configurations
* `gitops`: ArgoCD managed services and their configurations. Integrated with pipeline
* `todo-app`: Sample todo-app code base. Build, docker build, artifacts and deploy steps defined on pipeline.

## 3. Pipeline Configuration
Repo pipeline has 6 stages.
1. `utility-pre`: In this stage, we are running initial jobs to configure our ArgoCD server. Such as adding repositories and initializing needed namespaces. 
2. `utility-post`: In this stage, we are deploying our utility services to Kubernetes cluster. Such as `metrics-server`, `sealed-secrets`.
3. `build`: In this stage, we are installing dependencies and building our todo app.
4. `package`: In this stage, we are building a docker image and pushing the gitlab-registry. Docker image tag synced with `pipeline id`.
5. `prepare-artifacts`: In this stage, we are filling our custom helm chart manifests with desired values and storing those manifests on [Deployment Artifacts Repo](https://gitlab.com/kmtcloud/devops/deployment-artifacts).
6. `deploy`: In this stage, we are deploying our todo-app to Kubernetes cluster. 

You can see all the stages and defined jobs from the diagram.
![](images/pipeline-all.png)
### 3.1 Needed Pipeline Variables
* `ARGOCD_SERVER`: Endpoint of ArgoCD Server
* `ARGOCD_USERNAME`: Username to connect ArgoCD
* `ARGOCD_PASSWORD`: Password to connetc ArgoCD
* `ARGOCD_HEALTH_TIMEOUT_SECONDS`: Timeout value for ArgoCD sync process
* `AWS_ACCESS_KEY_ID`: AWS creds to add EKS clusters to ArgoCD
* `AWS_SECRET_ACCESS_KEY`: AWS creds to add EKS clusters to ArgoCD
* `CI_USERNAME`: Gitlab user to push manifests to Deployment Artifacts repo
* `CI_PUSH_TOKEN`: Gitlab user access token
* `GENERIC_HELM_CHART_VERSION`: Version of Generic helm chart that used to deploy todo-app
* `KMTCLOUD_CLUSTER_URL_EU_WEST1`: Cluster access url for eu-west-1 region
* `KMTCLOUD_CLUSTER_URL_EU_WEST2`: Cluster access url for eu-west-2 region
* `KMTCLOUD_DEPLOY_TOKEN_USERNAME`: Username to push docker images to Gitlab Registry
* `KMTCLOUD_DEPLOY_TOKEN_PASS`: Password to push docker images to Gitlab Registry
* `KMTCLOUD_DOCKER_IO_REGISTRY_AUTH_TOKEN`: Docker Hub Auth token
* `KMTCLOUD_REGISTRY_DOCKER_IO_USERNAME`: Docker hub user name
* `KMTCLOUD_REGISTRY_DOCKER_IO_PASSWORD`: Docker hub password
* `KMTCLOUD_REGISTRY_AUTH_TOKEN`: Gitlab Registry Auth Token
* `KMTCLOUD_REGISTRY_USERNAME`: Gitlab Registry username
* `KMTCLOUD_REGISTRY_PASSWORD`: Gitlab Registry password


### 3.2 Pipeline Jobs
* `add-cluster:eu-west-2`: By default, ArgoCD able to access and manage to the cluster that installed on. To manage applications on the failover cluster we need to trigger this job first.
* `add-repo:deployment-arfifacts`: Deployment artifacts repo contains our templated helm chart base manifests that created by `prepare-artifacts` jobs. To deploy todo-app on top of clusters with ArgoCD we need to give access permission to it.
* `add-repo:gitops`: We need to add this repo to ArgoCD also. 
* `deploy:namespaces`: This job deploys kubernetes namespaces with ArgoCD ApplicationSet resource.
* `deploy-metrics-server`: This job deploys metrics-server to the kubernetes clusters.
* `deploy-sealed-secrets`: This job deploys sealed-secrets to the kubernetes clusters.
* `deploy-sealed-secrets:extras`: This job apply custom sealed-secrets kubernetes service manifest file to the kubernetes clusters. Because we are facing issues to access embedded sealed-secrets-service via kubeseal to seal/unseal secrets.
* `npm:build`: To install dependencies and build the todo-app.
* `docker:build`: To build docker image of the todo-app and push it to the Gitlab Registry.
* `prepare-artifacts:qa-eu-west-1`: To create manifest files from custom helm chart and store the on Deployment Artifacts repository for eu-west-1 environment.
* `prepare-artifacts:qa-eu-west-2`: To create manifest files from custom helm chart and store the on Deployment Artifacts repository for eu-west-2 environment.
* `deploy:qa-eu-west-1`: To deploy the desired version of todo-app manifest to kubernetes cluster for eu-west-1 environment.
* `deploy:qa-eu-west-2`: To deploy the desired version of todo-app manifest to kubernetes cluster for eu-west-2 environment.


## 4. Terragrunt Configuration
[Terragrunt](https://terragrunt.gruntwork.io/) is a thin wrapper that provides extra tools for keeping your configurations DRY, working with multiple Terraform modules, and managing remote state.

`aws` folder contains all related resuorce configuration files managed by Terraform. The folders are separated by cloud provider, by account, by region, by environment, by resource groups and resources.
You can see all folder structure of `aws` folder as below:
```
aws
├── terragrunt.hcl
└── kadirt
    ├── account.hcl
    └── eu-west-1
        ├── region.hcl
        └── qa
            ├── env.hcl
            ├── eks
            │   ├── Makefile
            │   └── terragrunt.hcl
            ├── helm
            │   ├── argocd
            │   │   ├── Makefile
            │   │   └── terragrunt.hcl
            │   ├── cluster-auto-scaler
            │   │   ├── Makefile
            │   │   └── terragrunt.hcl
            │   ├── gitlab-runner
            │   │   ├── Makefile
            │   │   └── terragrunt.hcl
            │   └── ingress-nginx
            │       ├── Makefile
            │       └── terragrunt.hcl
            ├── iam
            │   ├── policies
            │   │   └── eks-policy
            │   │       ├── Makefile
            │   │       └── terragrunt.hcl
            │   └── roles
            │       ├── eks-role
            │       │   ├── Makefile
            │       │   └── terragrunt.hcl
            │       └── irsa-cas-role
            │           ├── Makefile
            │           └── terragrunt.hcl
            ├── networking
            │   ├── alb
            │   │   ├── Makefile
            │   │   └── terragrunt.hcl
            │   ├── alb-eks-attachment
            │   │   ├── module
            │   │   │   ├── main.tf
            │   │   │   ├── outputs.tf
            │   │   │   └── variables.tf
            │   │   ├── Makefile
            │   │   └── terragrunt.hcl
            │   │   
            │   ├── security-groups
            │   │   ├── alb
            │   │   │   ├── Makefile
            │   │   │   └── terragrunt.hcl
            │   │   └── db
            │   │       ├── Makefile
            │   │       └── terragrunt.hcl
            │   ├── vpc
            │   │   ├── Makefile
            │   │   └── terragrunt.hcl
            │   └── waf
            │       ├── alb-association
            │       │   ├── module
            │       │   │   ├── main.tf
            │       │   │   ├── outputs.tf
            │       │   │   └── variables.tf
            │       │   ├── Makefile
            │       │   └── terragrunt.hcl
            │       │
            │       └── wacl
            │           ├── Makefile
            │           └── terragrunt.hcl
            └── rds
                └── mysql
                    ├── Makefile
                    └── terragrunt.hcl
```
### 4.1 Core terragrunt files
* `aws/terragrunt.hcl` : Contains our provider configuration, remote state configuration and so on.
* `aws/${ACCOUNT_NAME}/account.hcl`: Contains account related information and configuration
* `aws/${ACCOUNT_NAME}/${REGION_NAME}/region.hcl`: Contains region related information and configuration
* `aws/${ACCOUNT_NAME}/${REGION_NAME}/${ENVIRONMENT_NAME}/env.hcl`: Contains environment related information and configuration

### 4.2 Terragrunt files for resources
* `aws/${ACCOUNT_NAME}/${REGION_NAME}/${ENVIRONMENT_NAME}/${RESOURCE_GROUP_NAME}/${RESOURCE_NAME}/terragrunt.hcl`: Contains resource configuration
* `aws/${ACCOUNT_NAME}/${REGION_NAME}/${ENVIRONMENT_NAME}/${RESOURCE_GROUP_NAME}/${RESOURCE_NAME}/Makefile`: Pre-defined make scripts


### 4.3 Resource Creation with Terragrunt
When creating the resources with Terragrunt, you need to go to the resource directory and run pre-defined make scripts.
```shell
cd aws/${ACCOUNT_NAME}/${REGION_NAME}/${ENVIRONMENT_NAME}/${RESOURCE_GROUP_NAME}/${RESOURCE_NAME}
#To see expected resources managed by Terragrunt
make plan
#To apply and create/update resources managed by Terragrunt
make apply
```

### 4.4 Resource Creation Order
Some resources that managed by Terragrunt are dependent each other. Because of that you need to create resources by order as follows.

#### 4.4.1 Main Environment

1. VPC on `aws/kadirt/eu-west-1/qa/networking/vpc`
2. Security Groups on `aws/kadirt/eu-west-1/qa/networking/security-groups/alb` and `aws/kadirt/eu-west-1/qa/networking/security-groups/db`
3. RDS on `aws/kadirt/eu-west-1/qa/rds/mysql` (if needed)
4. IAM Policy for EKS on `aws/kadirt/eu-west-1/qa/iam/policies/eks-policy`
5. IAM Role for EKS on `aws/kadirt/eu-west-1/qa/iam/roles/eks-role`
6. EKS Cluster on `aws/kadirt/eu-west-1/qa/eks`
7. ALB on `aws/kadirt/eu-west-1/qa/networking/alb`
8. ALB EKS Attachment on `aws/kadirt/eu-west-1/qa/networking/alb-eks-attachment`
9. IRSA Role on `aws/kadirt/eu-west-1/qa/iam/roles/irsa-cas-role`
10. Cluster Auto Scaler on `aws/kadirt/eu-west-1/qa/helm/cluster-auto-scaler`
11. Ingress Controller on `aws/kadirt/eu-west-1/qa/helm/ingress-nginx`
12. Gitlab Runner on `aws/kadirt/eu-west-1/qa/helm/gitlab-runner`
13. ArgoCD on `aws/kadirt/eu-west-1/qa/helm/argocd`
14. WAF ACL on `aws/kadirt/eu-west-1/qa/networking/waf/wacl`
15. WAF ALB Association on `aws/kadirt/eu-west-1/qa/networking/waf/alb-association`

#### 4.4.2 Failover Environment
1. VPC on `aws/kadirt/eu-west-2/qa/networking/vpc`
2. Security Groups on `aws/kadirt/eu-west-2/qa/networking/security-groups/alb`
3. IAM Policy for EKS on `aws/kadirt/eu-west-2/qa/iam/policies/eks-policy`
4. IAM Role for EKS on `aws/kadirt/eu-west-2/qa/iam/roles/eks-role`
5. EKS Cluster on `aws/kadirt/eu-west-2/qa/eks`
6. ALB on `aws/kadirt/eu-west-2/qa/networking/alb`
7. ALB EKS Attachment on `aws/kadirt/eu-west-2/qa/networking/alb-eks-attachment`
8. IRSA Role on `aws/kadirt/eu-west-2/qa/iam/roles/irsa-cas-role`
9. Cluster Auto Scaler on `aws/kadirt/eu-west-2/qa/helm/cluster-auto-scaler`
10. Ingress Controller on `aws/kadirt/eu-west-2/qa/helm/ingress-nginx`
11. Gitlab Runner on `aws/kadirt/eu-west-2/qa/helm/gitlab-runner`
12. WAF ACL on `aws/kadirt/eu-west-2/qa/networking/waf/wacl`
13. WAF ALB Association on `aws/kadirt/eu-west-2/qa/networking/waf/alb-association`

After configuration of failover environment:
1. Global Accelerator on `aws/kadirt/eu-west-1/qa/networking/global-accelerator`

