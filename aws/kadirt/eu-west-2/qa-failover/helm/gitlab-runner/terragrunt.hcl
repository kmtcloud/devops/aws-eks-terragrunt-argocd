locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  shared_tags      = local.environment_vars.locals.shared_tags
  app_name         = "gitlab-runner"
}

terraform {
  source = "github.com/terraform-module/terraform-helm-release.git?ref=v2.8.0"
}

include {
  path = find_in_parent_folders()
}


generate "kubernetes" {
  path      = "provider-helm.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<-EOF
data "aws_eks_cluster" "cluster" {
  name = "${dependency.eks.outputs.cluster_id}"
}
data "aws_eks_cluster_auth" "cluster" {
  name = "${dependency.eks.outputs.cluster_id}"
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
  }
}
  EOF
}

dependency "eks" {
  config_path = "../../eks"
}


inputs = {
  repository = "https://charts.gitlab.io"
  namespace  = "gitlab-runner"

  app = {
    name             = local.app_name
    chart            = "gitlab-runner"
    version          = "0.40.1"
    create_namespace = true
    wait             = true
    recreate_pods    = false
    deploy           = 1
  }

  values = [
    <<EOT
fullnameOverride: gitlab-runner
securityContext:
  fsGroup: 999
  runAsUser: 999
image: gitlab/gitlab-runner:ubuntu-v14.0.0
gitlabUrl: "https://gitlab.com"
runnerRegistrationToken: "#TODO:CHANGE ME"
unregisterRunners: true
terminationGracePeriodSeconds: 3600
concurrent: 50
checkInterval: 10
runners:
  privileged: "true"
  config: |
    [[runners]]
      request_concurrency = 40
      limit = 0
      environment = ["FF_GITLAB_REGISTRY_HELPER_IMAGE=1"]
      [runners.kubernetes]
        pull_policy = "if-not-present"
  env:
    GIT_SSL_NO_VERIFY: true
  tags: 'qa-eu-west-2'
  serviceAccountName: gitlab-runner
rbac:
  create: true
  clusterWideAccess: "true"
  rules:
   - apiGroups: ["*"]
     resources: ["deployment","pods", "secrets", "configmaps","roles","clusterroles", "rolebindings","clusterrolebindings"]
     verbs: ["get", "list", "watch", "create", "patch", "delete", "update"]
   - apiGroups: ["*"]
     resources: ["pods/exec"]
     verbs: ["create", "patch", "delete"]
   - apiGroups: ["*"]
     resources: ["pods/attach"]
     verbs: ["get", "list", "watch", "create", "patch", "delete", "update"]
   - apiGroups: ["argoproj.io"]
     resources: ["applicationsets","applications","appprojects"]
     verbs: ["get", "list", "watch", "create", "patch", "delete", "update"]
   - apiGroups: ["bitnami.com"]
     resources: ["sealedsecrets"]
     verbs: ["get", "list", "watch", "create", "patch", "delete", "update"]
   - apiGroups: ["*"]
     resources: ["services","services/proxy","endpoints",]
     verbs: ["get", "list", "watch", "create", "patch", "delete", "update"]
   - apiGroups: ["*"]
     resources: ["serviceaccounts","customresourcedefinitions"]
     verbs: ["get", "list", "watch", "create", "patch", "delete", "update"]
    EOT
  ]
}