locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  shared_tags      = local.environment_vars.locals.shared_tags
  cluster_name     = "${local.account_vars.locals.developer_name_valid}-${local.environment_vars.locals.environment}-eks"
}

terraform {
  source = "github.com/terraform-aws-modules/terraform-aws-eks?ref=v18.29.1"
}

include {
  path = find_in_parent_folders()
}

generate "kubernetes" {
  path      = "provider-k8s.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<-EOF
data "aws_eks_cluster" "cluster" {
  name = aws_eks_cluster.this[0].id
}
data "aws_eks_cluster_auth" "cluster" {
  name = aws_eks_cluster.this[0].id
}

provider "kubernetes" {
  host                   = aws_eks_cluster.this[0].endpoint
  cluster_ca_certificate = base64decode(aws_eks_cluster.this[0].certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}
  EOF
}

dependency "eks-iam-role" {
  config_path = "../iam/roles/eks-role"
}

dependency "vpc" {
  config_path = "../networking/vpc"
}

inputs = {
  create_eks                      = true
  cluster_name                    = local.cluster_name
  cluster_version                 = "1.23"
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
    }
    aws-ebs-csi-driver = {
      addonVersion      = "v1.11.4-eksbuild.1"
      resolve_conflicts = "OVERWRITE"
    }
  }

  vpc_id     = dependency.vpc.outputs.vpc_id
  subnet_ids = dependency.vpc.outputs.private_subnets

  # EKS Managed Node Group(s)
  eks_managed_node_group_defaults = {
    ami_type            = "AL2_x86_64"
    cluster_version     = "1.23"
    ami_release_version = "1.23.9-20220926"
    instance_types      = ["t3.medium"]
    disk_size           = 50

    attach_cluster_primary_security_group = true
  }

  eks_managed_node_groups = {
    default = {
      min_size     = 1
      max_size     = 5
      desired_size = 2

      instance_types = ["t3.large"]
      labels         = local.shared_tags

      update_config = {
        max_unavailable_percentage = 50 # or set `max_unavailable`
      }
      tags = local.shared_tags
    }
  }

  enable_irsa = true

  # aws-auth configmap
  manage_aws_auth_configmap = true

  aws_auth_node_iam_role_arns_non_windows = [
    dependency.eks-iam-role.outputs.iam_role_name
  ]

  aws_auth_users = [
    {
      userarn  = "arn:aws:iam::${local.account_vars.locals.aws_account_id}:root"
      username = "root"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::${local.account_vars.locals.aws_account_id}:user/delyant"
      username = "delyant"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::${local.account_vars.locals.aws_account_id}:user/muruvetk"
      username = "muruvetk"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::${local.account_vars.locals.aws_account_id}:user/jamilh"
      username = "jamilh"
      groups   = ["system:masters"]
    },
  ]

  tags = local.shared_tags
}

