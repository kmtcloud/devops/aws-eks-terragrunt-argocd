locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  env_name         = local.environment_vars.locals.environment
  region           = local.region_vars.locals.aws_region
  developer_name   = local.account_vars.locals.developer_name
  vpc_name         = "${local.developer_name}-vpc-${local.env_name}-${local.region}"

  vpc_configs = {
    eu-west-2 = {
      cidr-block = "10.20.0.0/16"
      zone-names = [
        "eu-west-2a",
        "eu-west-2b",
        "eu-west-2c"
      ]
      az-number = {
        a = 0
        b = 1
        c = 2
      }
      az-private-subnets = {
        a = "10.20.0.0/19"
        b = "10.20.32.0/19"
        c = "10.20.64.0/19"
      }
      az-public-subnets = {
        a = "10.20.100.0/24"
        b = "10.20.101.0/24"
        c = "10.20.102.0/24"
      }
      az-database-subnets = {
        a = "10.20.110.0/24"
        b = "10.20.111.0/24"
        c = "10.20.112.0/24"
      }
    }
  }
  shared_tags = local.environment_vars.locals.shared_tags
}

terraform {
  source = "git::https://github.com/terraform-aws-modules/terraform-aws-vpc.git//?ref=v3.16.0"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name = local.vpc_name
  cidr = local.vpc_configs[local.region].cidr-block
  azs  = local.vpc_configs[local.region].zone-names
  private_subnets = [
    local.vpc_configs[local.region].az-private-subnets[trimprefix(local.vpc_configs[local.region].zone-names[0], local.region)],
    local.vpc_configs[local.region].az-private-subnets[trimprefix(local.vpc_configs[local.region].zone-names[1], local.region)],
    local.vpc_configs[local.region].az-private-subnets[trimprefix(local.vpc_configs[local.region].zone-names[2], local.region)]
  ]
  public_subnets = [
    local.vpc_configs[local.region].az-public-subnets[trimprefix(local.vpc_configs[local.region].zone-names[0], local.region)],
    local.vpc_configs[local.region].az-public-subnets[trimprefix(local.vpc_configs[local.region].zone-names[1], local.region)],
    local.vpc_configs[local.region].az-public-subnets[trimprefix(local.vpc_configs[local.region].zone-names[2], local.region)]
  ]
  database_subnets = [
    local.vpc_configs[local.region].az-database-subnets[trimprefix(local.vpc_configs[local.region].zone-names[0], local.region)],
    local.vpc_configs[local.region].az-database-subnets[trimprefix(local.vpc_configs[local.region].zone-names[1], local.region)],
    local.vpc_configs[local.region].az-database-subnets[trimprefix(local.vpc_configs[local.region].zone-names[2], local.region)]
  ]

  create_database_subnet_group = true
  enable_dns_hostnames         = true
  enable_dns_support           = true
  enable_nat_gateway           = true
  single_nat_gateway           = true
  enable_vpn_gateway           = false
  enable_ipv6                  = false

  tags = local.shared_tags
  vpc_tags = {
    Name = local.vpc_name
  }
  public_subnet_tags = {
    tier                                                                                                                       = "public"
    "kubernetes.io/cluster/${local.account_vars.locals.developer_name_valid}-${local.environment_vars.locals.environment}-eks" = "shared"
    "kubernetes.io/role/elb"                                                                                                   = "1"
  }
  private_subnet_tags = {
    tier                                                                                                                       = "private"
    "kubernetes.io/cluster/${local.account_vars.locals.developer_name_valid}-${local.environment_vars.locals.environment}-eks" = "shared"
    "kubernetes.io/role/internal-elb"                                                                                          = "1"
  }
  database_subnet_tags = {
    tier = "db"
  }

}
