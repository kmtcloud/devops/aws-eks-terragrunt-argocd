resource "aws_autoscaling_attachment" "asg_alb_attachment" {
  autoscaling_group_name = var.autoscaling_group_name
  lb_target_group_arn    = var.lb_target_group_arn
}