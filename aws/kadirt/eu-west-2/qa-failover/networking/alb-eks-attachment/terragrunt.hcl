locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  shared_tags      = local.environment_vars.locals.shared_tags
}

terraform {
  source = "./module"
}

include {
  path = find_in_parent_folders()
}

dependency "eks" {
  config_path = "../../eks"
}

dependency "alb" {
  config_path = "../alb"
}

inputs = {
  autoscaling_group_name = dependency.eks.outputs.eks_managed_node_groups.default.node_group_autoscaling_group_names[0]
  lb_target_group_arn    = dependency.alb.outputs.target_group_arns[0]
}