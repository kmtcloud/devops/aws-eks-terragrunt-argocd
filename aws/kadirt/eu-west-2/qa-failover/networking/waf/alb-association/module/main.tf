resource "aws_wafv2_web_acl_association" "alb-association" {
  resource_arn = var.resource_arn
  web_acl_arn  = var.web_acl_arn
}