variable "resource_arn" {
  description = "ARN value of resources such as ALB, API Gateway"
}

variable "web_acl_arn" {
  description = "ARN value of WEB ACL"
}