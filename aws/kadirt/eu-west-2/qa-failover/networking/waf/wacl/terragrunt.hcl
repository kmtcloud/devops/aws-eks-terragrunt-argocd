locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  env_name         = local.environment_vars.locals.environment
  region           = local.region_vars.locals.aws_region
  developer_name   = local.account_vars.locals.developer_name_valid
  rule_name        = "${local.developer_name}-waf-rule-${local.env_name}-${local.region}"
  shared_tags      = local.environment_vars.locals.shared_tags
}

terraform {
  source = "github.com/cloudposse/terraform-aws-waf.git//?ref=0.0.4"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  rate_based_statement_rules = [
    {
      name     = local.rule_name
      action   = "block"
      priority = 10

      statement = {
        limit              = 100
        aggregate_key_type = "IP"
      }

      visibility_config = {
        cloudwatch_metrics_enabled = true
        sampled_requests_enabled   = true
        metric_name                = "waf-rate-metric"
      }
    }
  ]

}
