locals {
  environment = "qa"
  shared_tags = {
    Owner       = "kadir.taskiran"
    CreatedBy   = "Terraform"
    Environment = "${local.environment}"
    region      = "eu-west-1"
  }
}
