variable "lb_target_group_arn" {
  description = "ARN of ALB target group"
}

variable "autoscaling_group_name" {
  description = "Name of the ASG"
}