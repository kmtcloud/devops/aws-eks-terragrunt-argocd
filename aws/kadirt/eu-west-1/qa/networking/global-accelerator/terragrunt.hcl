locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  developer_name   = local.account_vars.locals.developer_name_valid
  region           = local.region_vars.locals.accelerator_region
  name             = "${local.developer_name}-accelerator-qa-${local.region}"
  shared_tags      = local.environment_vars.locals.shared_tags
  failover_alb_arn = "arn:aws:elasticloadbalancing:eu-west-2:995105043624:loadbalancer/app/kadirtaskiran-eks-alb-qa-fo/177f36c7430edf1e"
}

terraform {
  source = "github.com/terraform-aws-modules/terraform-aws-global-accelerator//?ref=v1.1.0"
}

include {
  path = find_in_parent_folders()
}

dependency "alb" {
  config_path = "../alb"
}


inputs = {
  name              = local.name
  flow_logs_enabled = false

  # Listeners
  listeners = {
    listener_1 = {
      client_affinity = "SOURCE_IP"

      endpoint_group = {
        health_check_port             = 80
        health_check_protocol         = "HTTP"
        health_check_path             = "/healthz"
        health_check_interval_seconds = 10
        health_check_timeout_seconds  = 5
        healthy_threshold_count       = 2
        unhealthy_threshold_count     = 2
        traffic_dial_percentage       = 100

        endpoint_configuration = [{
          client_ip_preservation_enabled = true
          endpoint_id                    = dependency.alb.outputs.lb_arn
          weight                         = 100
          }, {
          client_ip_preservation_enabled = true
          endpoint_id                    = local.failover_alb_arn
          weight                         = 0
        }]
      }

      port_ranges = [
        {
          from_port = 80
          to_port   = 80
        }
      ]
      protocol = "TCP"
    }
  }

  listeners_timeouts = {
    create = "35m"
    update = "35m"
    delete = "35m"
  }

  endpoint_groups_timeouts = {
    create = "35m"
    update = "35m"
    delete = "35m"
  }

  tags = local.shared_tags

}
