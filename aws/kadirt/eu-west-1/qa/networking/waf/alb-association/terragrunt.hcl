locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  shared_tags      = local.environment_vars.locals.shared_tags
}

terraform {
  source = "./module"
}

include {
  path = find_in_parent_folders()
}


dependency "alb" {
  config_path = "../../alb"
}


dependency "wacl" {
  config_path = "../wacl"
}

inputs = {
  web_acl_arn  = dependency.wacl.outputs.arn
  resource_arn = dependency.alb.outputs.lb_arn
}