locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  alb_name         = "${local.account_vars.locals.developer_name_valid}-eks-alb"
  shared_tags      = local.environment_vars.locals.shared_tags
}

terraform {
  source = "github.com/terraform-aws-modules/terraform-aws-alb.git?ref=v8.1.0"
}

include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../vpc"
}

dependency "alb-sg" {
  config_path = "../security-groups/alb"
}

dependency "eks" {
  config_path = "../../eks"
}

inputs = {

  name = local.alb_name

  load_balancer_type = "application"

  vpc_id = dependency.vpc.outputs.vpc_id
  security_groups = [
    dependency.alb-sg.outputs.security_group_id,
    dependency.eks.outputs.cluster_primary_security_group_id
  ]
  subnets = dependency.vpc.outputs.public_subnets

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
      # action_type        = "forward"
    }
  ]

  target_groups = [
    {
      name_prefix          = "http"
      backend_protocol     = "HTTP"
      backend_port         = 31080
      target_type          = "instance"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/healthz"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 3
        timeout             = 6
        protocol            = "HTTP"
        matcher             = "200-399"
      }
      protocol_version = "HTTP1"

    },

  ]

  tags = local.shared_tags
}
