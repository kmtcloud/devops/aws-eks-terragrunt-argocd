locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  env_name         = local.environment_vars.locals.environment
  region           = local.region_vars.locals.aws_region
  developer_name   = local.account_vars.locals.developer_name_valid
  identifier_name  = "${local.developer_name}-db-${local.env_name}-${local.region}"
  db_name          = "${local.developer_name}db"
  db_user          = "${local.developer_name}"
  shared_tags      = local.environment_vars.locals.shared_tags
}

terraform {
  source = "git::https://github.com/terraform-aws-modules/terraform-aws-rds.git//?ref=v5.1.0"
}

dependency "vpc" {
  config_path = "../../networking/vpc"
}

dependency "sg" {
  config_path = "../../networking/security-groups/db"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  identifier             = local.identifier_name
  db_name                = local.db_name
  username               = local.db_user
  port                   = 3306
  multi_az               = true
  subnet_ids             = dependency.vpc.outputs.database_subnets
  vpc_security_group_ids = [dependency.sg.outputs.security_group_id]
  instance_class         = "db.t3.small"
  db_subnet_group_name   = dependency.vpc.outputs.database_subnet_group_name

  engine               = "mysql"
  engine_version       = "8.0.27"
  family               = "mysql8.0"
  major_engine_version = "8.0"

  allocated_storage     = 20
  max_allocated_storage = 100

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  backup_retention_period = 0
  skip_final_snapshot     = true
  deletion_protection     = false

  performance_insights_enabled = false
  create_monitoring_role       = false

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8mb4"
    },
    {
      name  = "character_set_server"
      value = "utf8mb4"
    }
  ]

  tags = local.shared_tags
  db_instance_tags = {
    "Sensitive" = "high"
  }
  db_option_group_tags = {
    "Sensitive" = "low"
  }
  db_parameter_group_tags = {
    "Sensitive" = "low"
  }
  db_subnet_group_tags = {
    "Sensitive" = "high"
  }
}
