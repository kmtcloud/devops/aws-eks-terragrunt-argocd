locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  developer_name   = local.account_vars.locals.developer_name
  env_name         = local.environment_vars.locals.environment
  role_name        = "${local.developer_name}-eks-role-${local.env_name}"
  shared_tags      = local.environment_vars.locals.shared_tags
}

terraform {
  source = "github.com/terraform-aws-modules/terraform-aws-iam.git//modules/iam-assumable-role?ref=v5.3.3"
}

include {
  path = find_in_parent_folders()
}

dependency "eks-policy" {
  config_path = "../../policies/eks-policy"
}

inputs = {

  trusted_role_services = [
    "ec2.amazonaws.com",
    "eks.amazonaws.com",
  ]

  trusted_role_arns = [
  ]

  create_role       = true
  role_name         = "${local.role_name}"
  role_requires_mfa = false

  custom_role_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy",
    "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
    "arn:aws:iam::aws:policy/AmazonEKSServicePolicy",
    "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy",
    dependency.eks-policy.outputs.arn
  ]

  #tags = local.shared_tags
}
