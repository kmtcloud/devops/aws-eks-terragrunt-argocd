locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  developer_name   = local.account_vars.locals.developer_name
  env_name         = local.environment_vars.locals.environment
  role_name        = "${local.developer_name}-cas-irsa-role-${local.env_name}"
  shared_tags      = local.environment_vars.locals.shared_tags
}

terraform {
  source = "github.com/terraform-aws-modules/terraform-aws-iam.git//modules/iam-role-for-service-accounts-eks?ref=v5.3.3"
}

include {
  path = find_in_parent_folders()
}

dependency "eks" {
  config_path = "../../../eks"
}

inputs = {

  role_name                        = local.role_name
  attach_cluster_autoscaler_policy = true
  cluster_autoscaler_cluster_ids   = [dependency.eks.outputs.cluster_id]

  oidc_providers = {
    ex = {
      provider_arn               = dependency.eks.outputs.oidc_provider_arn
      namespace_service_accounts = ["kube-system:cluster-autoscaler"]
    }
  }

  tags = local.shared_tags
}
