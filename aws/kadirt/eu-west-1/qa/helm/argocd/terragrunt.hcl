locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  shared_tags      = local.environment_vars.locals.shared_tags
  app_name         = "argocd"
}

terraform {
  source = "github.com/terraform-module/terraform-helm-release.git?ref=v2.8.0"
}

include {
  path = find_in_parent_folders()
}


generate "kubernetes" {
  path      = "provider-helm.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<-EOF
data "aws_eks_cluster" "cluster" {
  name = "${dependency.eks.outputs.cluster_id}"
}
data "aws_eks_cluster_auth" "cluster" {
  name = "${dependency.eks.outputs.cluster_id}"
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
  }
}
  EOF
}

dependency "eks" {
  config_path = "../../eks"
}


inputs = {
  repository = "https://argoproj.github.io/argo-helm"
  namespace  = "argocd"

  app = {
    name             = local.app_name
    chart            = "argo-cd"
    version          = "5.5.19"
    create_namespace = true
    wait             = true
    recreate_pods    = false
    deploy           = 1
  }

  values = [
    <<EOT
fullnameOverride: "argocd"

## Server
server:
  name: server
  replicas: 1

  extraArgs:
    - --insecure

  service:
    type: ClusterIP

  ingress:
    enabled: true
    annotations: {}
    ingressClassName: "nginx"
    hosts:
      - argocd.kmtcloud.com
    paths:
      - /
    pathType: Prefix
    tls: []
    https: false

  configEnabled: true
  config:
    url: "argocd.kmtcloud.com"
    application.instanceLabelKey: argocd.argoproj.io/instance

  clusterAdminAccess:
    enabled: true

## Repo Server
repoServer:
  name: repo-server
  replicas: 1

## Argo Configs
configs:
  clusterCredentials: []

  credentialTemplates: {}
  secret:
    # -- Create the argocd-secret
    createSecret: true
    # -- Argo TLS Data
    argocdServerTlsConfig: {}
applicationSet:
  # -- Enable Application Set controller
  enabled: true

notifications:
  # -- Enable Notifications controller
  enabled: true

    EOT
  ]
}