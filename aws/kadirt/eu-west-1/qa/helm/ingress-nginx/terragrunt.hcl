locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  shared_tags      = local.environment_vars.locals.shared_tags
  app_name         = "ingress-nginx"
}

terraform {
  source = "github.com/terraform-module/terraform-helm-release.git?ref=v2.8.0"
}

include {
  path = find_in_parent_folders()
}

dependency "eks" {
  config_path = "../../eks"
}

generate "kubernetes" {
  path      = "provider-helm.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<-EOF
data "aws_eks_cluster" "cluster" {
  name = "${dependency.eks.outputs.cluster_id}"
}
data "aws_eks_cluster_auth" "cluster" {
  name = "${dependency.eks.outputs.cluster_id}"
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
  }
}
  EOF
}

inputs = {
  repository = "https://kubernetes.github.io/ingress-nginx"
  namespace  = "ingress-nginx"

  app = {
    name             = local.app_name
    chart            = "ingress-nginx"
    version          = "4.3.0"
    create_namespace = true
    wait             = true
    recreate_pods    = false
    deploy           = 1
  }

  values = [
    <<EOT
controller:
  affinity:
    podAntiAffinity:
       preferredDuringSchedulingIgnoredDuringExecution:
       - weight: 100
         podAffinityTerm:
           labelSelector:
             matchExpressions:
             - key: app.kubernetes.io/name
               operator: In
               values:
               - ingress-nginx
           topologyKey: kubernetes.io/hostname

  autoscaling:
    enabled: true
    minReplicas: 2
    maxReplicas: 5
    targetCPUUtilizationPercentage: 70
    targetMemoryUtilizationPercentage: 70
  service:
    type: NodePort
    nodePorts:
      http: 31080
      https: 31081
    EOT
  ]
}