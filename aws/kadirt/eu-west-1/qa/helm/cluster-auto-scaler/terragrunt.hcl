locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  shared_tags      = local.environment_vars.locals.shared_tags
  app_name         = "aws-acs"
}

terraform {
  source = "github.com/terraform-module/terraform-helm-release.git?ref=v2.8.0"
}

include {
  path = find_in_parent_folders()
}


generate "kubernetes" {
  path      = "provider-helm.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<-EOF
data "aws_eks_cluster" "cluster" {
  name = "${dependency.eks.outputs.cluster_id}"
}
data "aws_eks_cluster_auth" "cluster" {
  name = "${dependency.eks.outputs.cluster_id}"
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
  }
}
  EOF
}

dependency "eks" {
  config_path = "../../eks"
}

dependency "irsa-role" {
  config_path = "../../iam/roles/irsa-cas-role"
}

inputs = {
  repository = "https://kubernetes.github.io/autoscaler"
  namespace  = "kube-system"

  app = {
    name             = local.app_name
    chart            = "cluster-autoscaler"
    version          = "9.21.0"
    create_namespace = false
    wait             = true
    recreate_pods    = false
    deploy           = 1
  }

  set = [
    {
      name  = "fullnameOverride"
      value = "cluster-autoscaler"
    },
    {
      name  = "autoDiscovery.clusterName"
      value = dependency.eks.outputs.cluster_id
    },
    {
      name  = "rbac.serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
      value = dependency.irsa-role.outputs.iam_role_arn
    },
  ]
}